from django import forms
from django.forms import TextInput, EmailField, EmailInput, Textarea

from contact.models import Contact


class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = '__all__'
        widgets = {
            'name': TextInput(attrs={'placeholder': 'Please enter your name', 'class': 'form-control'}),
            'email': EmailInput(attrs={'placeholder': 'Please enter your email', 'class': 'form-control'}),
            'message': Textarea(attrs={'placeholder': 'Please enter your message, I will respond as soon as possible', 'class': 'form-control'})
        }