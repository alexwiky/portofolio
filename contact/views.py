from django.core.mail import send_mail
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView

from contact.forms import ContactForm
from contact.models import Contact


class ContactTemplateView(CreateView):
    template_name = '../templates/contact/contact.html'
    model = Contact
    form_class = ContactForm
    success_url = reverse_lazy('index')

    def form_valid(self, form):
            if form.is_valid():
                user = form.save(commit=False)
                user.save()
                send_mail(subject='Contacted by website',
                          message=f'Mail sent from: {user.email}, Name: {user.name}, Message: {user.message}',
                          from_email='no-reply@wichingeralex.ro',
                          recipient_list=['alexwiky@gmail.com'])
            return redirect('index')
